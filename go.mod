module invent.kde.org/websites/hugo-kde

go 1.14

require (
	github.com/thednp/bootstrap.native v0.0.0-20220121213928-48c5590165ff // indirect
	invent.kde.org/websites/hugo-bootstrap v0.0.0-20230310220713-2c58a2e72e20 // indirect
)
